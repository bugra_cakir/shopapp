import 'package:shop_app/providers/Entities/order_item.dart';

import 'Entities/cart_item.dart';

class Orders with ChangeNotifier {
  List<OrderItem> _orders = [];

  List<OrderItem> get orders {
    return [..._orders]; //copy of orders
  }

  void addOrder(List<CartItem> cartProducts, double total) {
    //add the last order to the orderlist head
    //making id uniqe with this operation
    _orders.insert(
      0,
      OrderItem(
          id: DateTime.now().toString(),
          amount: total,
          dateTime: DateTime.now(),
          orderProducts: cartProducts),
    );
    notifyListeners();
  }
}
